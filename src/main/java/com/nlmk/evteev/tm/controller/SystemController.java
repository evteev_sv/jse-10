package com.nlmk.evteev.tm.controller;

/**
 * Класс контроллера системных действий
 */
public class SystemController {


    /**
     * Показ сообщения об ошибке
     */
    public int displayError() {
        System.out.println("Неподдерживаемый аргумент. наберите команду " +
            "help для получения списка доступных команд...");
        return -1;
    }

    /**
     * Показ сведений о ключах
     */
    public int displayHelp() {
        System.out.println("version - Информация о версии.");
        System.out.println("about - Информация о разработчике.");
        System.out.println("help - Информация о доступных командах.");
        System.out.println("exit - Завершение работы приложения");

        System.out.println("proj-create - Создание проекта");
        System.out.println("proj-clear - Очистка проектов");
        System.out.println("proj-list - Вывод списка проектов");
        System.out.println("proj-view - Просмотр проектов");
        System.out.println("proj-remove-by-name - Удаление проекта по имени");
        System.out.println("proj-remove-by-id - Удаление проекта по коду");
        System.out.println("proj-remove-by-index - Удаление проекта по индексу");
        System.out.println("proj-update-by-index - Изменение проекта по индексу");
        System.out.println("proj-update-by-id - Изменение проекта по коду");
        System.out.println("proj-add-task-by-ids - Добавление задачи к проекту по коду");
        System.out.println("proj-remove-task-by-ids - Удаление задачи из проекта");
        System.out.println("proj-remove-tasks - Удаление всех задач из проекта");
        System.out.println("proj-remove-with-tasks - Удаление проекта вместе с задачами");

        System.out.println("task-create - Создание задачи");
        System.out.println("task-clear - Очистка задачи");
        System.out.println("task-list - Вывод списка задач");
        System.out.println("task-view - Просмотр задачи");
        System.out.println("task-remove-by-id - Удаление задачи по коду");
        System.out.println("task-remove-by-name - Удаление задачи по имени");
        System.out.println("task-remove-by-index - Удаление задачи по индексу");
        System.out.println("task-update-by-index - Изменение задачи по индексу");
        System.out.println("task-update-by-id - Изменение задачи по коду");
        System.out.println("task-view-without-project - Просмотр задач, непривязанных к проектам");
        System.out.println("task-view-by-project - Просмотр задач по коду проекта");
        return 0;
    }

    /**
     * Показ сведений о версиях
     */
    public int displayVersion() {
        System.out.println("1.0.5");
        return 0;
    }

    /**
     * Показ сведений об авторе
     */
    public int displayAbout() {
        System.out.println("Author: Sergey Evteev");
        System.out.println("e-mail: sergey@evteev.ru");
        return 0;
    }

    /**
     * Показ приветствия
     */
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Стандартный выход
     */
    public void displayExit() {
        System.out.println("Получена команда завершения работы...");
        System.exit(0);
    }

}
