package com.nlmk.evteev.tm.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, описывающий проект
 */
public class Project {

    private Long id = System.nanoTime();
    private String name = "";
    private String description = "";
    private final List<Long> tasks = new ArrayList<>();

    /**
     * Конструктор по умолчанию
     */
    public Project() {
    }

    /**
     * Создание класса
     *
     * @param pName имя проекта
     */
    public Project(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getTasks() {
        return tasks;
    }

    @Override
    public String toString() {
        return "Project {id=" + id + ", name=" + name + "}";
    }

}
